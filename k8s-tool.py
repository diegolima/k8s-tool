import os
import json
from time import sleep

class Gcloud:
  def __init__(self,configurations=[]):
    self.__cmd='gcloud --format=json '
    self.__kubecmd='kubectl -o json '
    self.configurations=self.__get_configurations()
    self.config={}
    self.config['configurations']=configurations

    for configuration in self.config['configurations']:
      if not self.__get_configuration(configuration):
        self.__create_configuration(configuration)
        self.__init_configuration(configuration)

  def __create_configuration(self, configuration):
    print('Criando configuracao: ' + configuration)
    if os.system(self.__cmd + "config configurations create " + configuration) == 0:
      return(True)
    else:
      return(False)

  def __delete_configuration(self, configuration):
    print('Removendo configuracao: ' + configuration)
    if os.system(self.__cmd + "config configurations delete " + configuration) == 0:
      return(True)
    else:
      return(False)

  def __init_configuration(self, configuration):
    print('Inicializando configuracao para o ambiente ' + configuration)
    print('Quando solicitado escolha o projeto equivalente!')
    if os.system(self.__cmd + "--configuration " + configuration + " init --console-only") == 0:
      return(True)
    else:
      return(False)

  def __get_configurations(self):
    result=json.loads(os.popen(self.__cmd + "config configurations list").read())
    return(result)

  def __get_configuration(self, configuration):
    for item in self.configurations:
      if item['name'] == configuration:
        return(True)
    return(False)

  def __get_clusters(self):
    result=json.loads(os.popen(self.__cmd + "container clusters list").read())
    return(result)

  def __get_cluster(self):
    result=json.loads(os.popen(self.__cmd + "container clusters list").read())[0]
    return(result)

  def __activate_configuration(self, configuration):
    if os.popen(self.__cmd + "config configurations activate " + configuration):
      sleep(2)
      self.configuration=configuration
      self.config[configuration]={}
      self.config[configuration]['cluster']=self.__get_cluster()
      if os.popen(self.__cmd + "container clusters get-credentials " + self.config[configuration]['cluster']['name'] + " --zone " + self.config[configuration]['cluster']['zone']).read():
        print("Utilizando cluster " + self.config[configuration]['cluster']['name'])
        self.config[configuration]['namespaces']=json.loads(os.popen("kubectl get ns -o json").read())['items']
        return(True)
    else:
      return(False)

  def __activate_namespace(self, namespace):
    print("Utilizando namespace " + namespace)
    self.config[self.configuration]['namespace']=namespace

  def __load_pod_list(self):
    self.config[self.configuration]['pods']=json.loads(os.popen(self.__kubecmd + '-n ' + self.config[self.configuration]['namespace'] + ' get pods').read())['items']

  def __get_namespaces(self):
    return(self.config[self.configuration]['namespaces'])

  def __get_pods(self):
    self.__load_pod_list()
    return(self.config[self.configuration]['pods'])

  def __get_pod_apps(self):
    pods=self.__get_pods()
    labels=[]
    for pod in pods:
      labels.append(pod['metadata']['labels'].get('app'))
    labels=list(dict.fromkeys(labels))
    return labels

  def select_configuration(self):
    i=0
    conf_menu={}
    for configuration in self.config['configurations']:
      conf_menu[str(i)] = configuration
      i=i+1

    selection = self.__showMenu(conf_menu)
    self.__activate_configuration(conf_menu[selection])

  def select_namespace(self):
    namespaces=self.__get_namespaces()
    ns_menu={}
    i=0
    for namespace in namespaces:
      ns_menu[str(i)] = namespace['metadata']['name']
      i=i+1
    selection = self.__showMenu(ns_menu)
    self.__activate_namespace(ns_menu[selection])

  def select_action(self):
    action_menu={}
    action_menu['1'] = 'Selecionar aplicacao'
    if self.config[self.configuration].get('current_app'):
      action_menu['2'] = 'Ver logs de ' + self.config[self.configuration]['current_app']
    if self.config[self.configuration].get('current_app'):
      action_menu['3'] = 'Ver log stream de ' + self.config[self.configuration]['current_app']
    action_menu['N'] = 'Alterar namespace'
    action_menu['Q'] = 'Sair'

    print('')
    selection = self.__showMenu(action_menu)
    if selection == '1':
      app_selection_menu={}
      i=0
      for app in self.__get_pod_apps():
        if app:
          app_selection_menu[str(i)] = app
          i=i+1
      app_selection_menu['R'] = 'Retornar'
      print("Selecione a aplicacao:")
      selection = self.__showMenu(app_selection_menu)
      if selection.lower() != 'r':
        self.config[self.configuration]['current_app'] = app_selection_menu[selection]
    elif selection == '2' and self.config[self.configuration].get('current_app'):
      self.__show_app_logs()
    elif selection == '3' and self.config[self.configuration].get('current_app'):
      self.__stream_app_logs()
    elif selection.lower() == 'n':
      self.select_namespace()
    elif selection.lower() == 'q':
      return(False)
    else:
      return(True)
    return(True)

  def __show_app_logs(self):
    os.system('kubectl -n ' + self.config[self.configuration]['namespace'] + ' logs -l app=' + self.config[self.configuration]['current_app'])

  def __stream_app_logs(self):
    print('Carregando log stream de ' + self.config[self.configuration]['current_app'] +'. Pressione CTRL+C para finalizar.')
    sleep(3)
    os.system('kubectl wait --for=condition=ContainersReady --timeout=60s pod -n ' + self.config[self.configuration]['namespace'] + ' --selector app=' + self.config[self.configuration]['current_app'])
    os.system('kubectl -n ' + self.config[self.configuration]['namespace'] + ' logs -f --max-log-requests=999 -l app=' + self.config[self.configuration]['current_app'])

  def __showMenu(self, items, sort=False):
    options = list(items.keys())
    if sort:
      options.sort()
    for entry in options:
      print(entry + " - " + items[entry])
    choice = input("Escolha uma opcao:\n")
    return(choice)

def main():
  configurations=['pontotel-prod', 'pontotel-hmg', 'pontotel-dev']
  client=Gcloud(configurations=configurations)

  print("Selectione a configuracao que deseja utilizar:")
  client.select_configuration()

  print("Selectione o namespace:")
  client.select_namespace()

  while client.select_action():
    pass

  exit(0)

if __name__ == "__main__":
  # Python 2-3 compatibility workaround
  try:
    input = raw_input
  except NameError:
    pass
  main()